import React, { memo } from 'react';
import { TouchableOpacity, View } from 'react-native';

import { useCalendar } from '../../hooks';

import Text from '../Text';

import styles from './styles';
import { THIS_MONTH } from '../../constants';

const Day = ({
  day,
  monthIndex,
  accessibilityLabel,
  isPressed = false,
  isDisabled = false
}) => {
  const dispatch = useCalendar();

  const onPress = () => {
    if (isDisabled) return;

    if (monthIndex === THIS_MONTH) {
      dispatch({
        type: 'SET_SELECTED_DATE',
        payload: {
          day: day
        }
      });
    } else {
      dispatch({
        type: 'SET_MONTH',
        payload: {
          month: monthIndex,
          day: day
        }
      });
    }
  };

  const color = isDisabled
    ? 'yellow'
    : monthIndex !== THIS_MONTH
    ? 'purple'
    : 'white';
  const backgroundColor = isPressed ? 'blue' : 'transparent';

  return (
    <View style={styles.container} testID="container">
      {day !== null && (
        <TouchableOpacity
          style={[styles.button, { backgroundColor }]}
          isPressed={isPressed}
          onPress={onPress}
          disabled={isDisabled}
          accessibilityLabel={accessibilityLabel}
          testID="button"
        >
          <Text color={color}>{day}</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default memo(Day);
