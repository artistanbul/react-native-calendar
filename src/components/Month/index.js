import React, { useState, useLayoutEffect, memo, useMemo } from 'react';
import { View } from 'react-native';

import { THIS_MONTH, NEXT_MONTH, PREV_MONTH } from '../../constants';
import getDaysOfMonth from '../../utils/getDaysOfMonth';

import Day from '../Day';

import styles from './styles';


const Month = ({ state, ...props }) => {
  const { selectedDate, currentMonth, currentYear } = state;
  const [days, setDays] = useState();
  const allowedDates = useMemo(() => (
    props.allowedDates !== undefined &&
    props.allowedDates
    .filter(date => date.getMonth() === currentMonth)
    .map(date => date.getDate())
  ), [props.allowedDates, currentMonth]);

  useLayoutEffect(() => {
    const days = getDaysOfMonth(
      currentYear,
      currentMonth,
      props.firstDayOfTheWeek
    );
    setDays(days);
  }, [
    currentYear,
    currentMonth,
    props.firstDayOfTheWeek,
    setDays
  ]);

  const getAccessibilityLabel = date => {
    return date.toString();
  }

  if (!days)  return null;

  const daysMissing = (
    props.showNextMonth &&
    props.forceShowSixWeeks &&
    days.prevOffset + days.dayCount + days.nextOffset !== 7 * 6 &&
    (7 * 6) - (days.prevOffset + days.dayCount + days.nextOffset)
  );

  return (
    <View style={styles.month}>
      {props.showPreviousMonth &&
        [...Array(days.prevOffset)
          .fill()
          .map((_, idx) => days.prevMonthDayCount - days.prevOffset + 1 + idx)
        ]
          .map(day =>
            <Day
              key={day}
              day={day}
              monthIndex={PREV_MONTH}
            />
          )
      }
      {[...Array(days.dayCount + 1).keys()].slice(1).map(day => {
          const isPressed = (
            day === selectedDate.getDate() &&
            currentMonth === selectedDate.getMonth() &&
            currentYear === selectedDate.getFullYear()
          );
          const isDisabled = allowedDates && !allowedDates.includes(day);
          const accessibilityLabel = getAccessibilityLabel(new Date(currentYear, currentMonth, day));

          return (
            <Day
              key={day}
              day={day}
              isPressed={isPressed}
              isDisabled={isDisabled}
              monthIndex={THIS_MONTH}
              accessibilityLabel={accessibilityLabel}
            />
          );
        }
      )}
      {
        props.showNextMonth &&
        [...Array(days.nextOffset + 1).keys()]
        .slice(1)
        .map(day =>
          <Day
            key={day}
            day={day}
            monthIndex={NEXT_MONTH}
          />
        )
      }
      {
        daysMissing &&
        [...Array(daysMissing + 1).keys()]
          .slice(1)
          .map(day =>
            <Day
              key={day + days.nextOffset}
              day={day + days.nextOffset}
              monthIndex={NEXT_MONTH}
            />
          )
      }
    </View>
  );
}

export default memo(Month);
