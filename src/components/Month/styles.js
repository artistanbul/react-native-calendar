import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  month: {
    flex: 0.85,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start'
  }
});

export default styles;
