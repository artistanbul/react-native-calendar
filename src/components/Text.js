import React from 'react';
import { Text as RNText } from 'react-native';


const Text = ({ children, color, align, ...props }) => {
  return (
    <RNText
      style={{
        color,
        textAlign: align
      }}
      {...props}
    >
      {children}
    </RNText>
  );
}

Text.defaultProps = {
  color: 'white',
  textAlign: 'center'
}

export default Text;
