import { DAYS } from '../constants';

function getDaysOfMonth(year, month, firstDayOfTheWeek) {
  const dayCount = new Date(year, month + 1, 0).getDate();
  const dateOfFirstDay = new Date(year, month, 1).getDay();
  const prevMonthDayCount = new Date(year, month, 0).getDate();
  const lastDayOfTheCurrentMonth = new Date(year, month + 1, 0).getDay();
  const lastDayOfTheWeek = (
    firstDayOfTheWeek === DAYS.SUNDAY ?
    DAYS.SATURDAY :
    firstDayOfTheWeek - 1
  );

  let prevOffset = 0;

  if (dateOfFirstDay !== firstDayOfTheWeek) {
    prevOffset = (
      dateOfFirstDay - firstDayOfTheWeek > 0 ?
      dateOfFirstDay - firstDayOfTheWeek :
      (7 + dateOfFirstDay) - firstDayOfTheWeek
    );
  }

  const arr = [...Array(7).keys()];
  arr.unshift.apply(arr, arr.splice(7 - lastDayOfTheCurrentMonth, 7));
  const nextOffset = arr[lastDayOfTheWeek];

  return {
    dayCount,
    prevOffset,
    nextOffset,
    prevMonthDayCount
  }
}

export default getDaysOfMonth;
