import isDateObject from '../src/utils/isDateObject';

describe('isDateObject', () => {
    it('should return true when value is a date object', () => {
        expect(isDateObject(new Date())).toBeTruthy();
        expect(isDateObject(new Date(2020, 1, 13))).toBeTruthy();
    });

    it('should return false when value is not a date object', () => {
        expect(isDateObject(null)).toBeFalsy();
        expect(isDateObject(undefined)).toBeFalsy();
        expect(isDateObject(1)).toBeFalsy();
        expect(isDateObject(0)).toBeFalsy();
        expect(isDateObject('')).toBeFalsy();
        expect(isDateObject({})).toBeFalsy();
        expect(isDateObject({ test: 'test' })).toBeFalsy();
        expect(isDateObject(() => null)).toBeFalsy();
    });
});