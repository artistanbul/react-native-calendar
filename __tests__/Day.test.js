import React, { useReducer } from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import { renderHook } from '@testing-library/react-hooks';

import { reducer } from '../src/components/Calendar';
import { PREV_MONTH, THIS_MONTH, NEXT_MONTH } from '../src/constants';

import Day from '../src/components/Day';
import CalendarContext from '../src/context';
/**
jest.mock('../src/hooks', () => {
  return {
    useCalendar: jest.fn(() => {
      return ({
        type,
        payload
      }) => console.log("mocked");
    })
  };
});
 */

MY_CONTEXT_MOCK = {
  selectedDate: new Date(),
  currentMonth: new Date().getMonth(),
  currentYear: new Date().getFullYear()
};

describe('Day component', () => {
  it('renders', () => {
    const day = 1;
    const monthIndex = THIS_MONTH;

    const { queryByText } = render(<Day day={day} monthIndex={monthIndex} />);
    const text = queryByText('1');

    expect(text).not.toBe(null);
    expect(text.props.style.color).toBe('white');
  });

  it('renders', async () => {
    const day = 1;
    const monthIndex = PREV_MONTH;
    const accessibilityLabel = new Date().setDate(day).toString();

    const { result, waitForNextUpdate } = renderHook(() =>
      useReducer(reducer, MY_CONTEXT_MOCK)
    );
    const [state, dispatch] = result.current;

    const { queryByText, getByTestId } = render(
      <CalendarContext.Provider value={args => dispatch(args)}>
        <Day
          day={day}
          monthIndex={monthIndex}
          accessibilityLabel={accessibilityLabel}
        />
      </CalendarContext.Provider>
    );
    const text = queryByText('1');
    const button = getByTestId('button');

    expect(text).not.toBe(null);
    expect(text.props.style.color).toBe('purple');
    expect(button.props.accessibilityLabel).toBe(accessibilityLabel);

    fireEvent.press(button);

    //hooks.useCalendar.mockReset();
  });

  it('renders', () => {
    const day = 1;
    const monthIndex = NEXT_MONTH;

    const { queryByText } = render(<Day day={day} monthIndex={monthIndex} />);
    const text = queryByText('1');

    expect(text).not.toBe(null);
    expect(text.props.style.color).toBe('purple');
  });
});
