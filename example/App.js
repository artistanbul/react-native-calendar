import React, { useState } from 'react';
import { SafeAreaView, View, StyleSheet, StatusBar, Text, Button } from 'react-native';

import Calendar from 'react-native-calendar';


const App = () => {
  const [selectedMonth, setSelectedMonth] = useState();
  const [selectedYear, setSelectedYear] = useState();
  const [selectedDate, setSelectedDate] = useState();
  const [dat, setDat] = useState(new Date(2020, 5, 10));

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.wrapper}>
            <Calendar
              onMonthChange={month => setSelectedMonth(month)}
              onYearChange={year => setSelectedYear(year)}
              onDateSelected={date => setSelectedDate(date)}
              currentDate={dat}
              forceShowSixWeeks={true}
              showPreviousMonth={true}
              showNextMonth={true}
            />
          </View>
          <View style={styles.info}>
            <Text>Selected Month: {selectedMonth}</Text>
            <Text>Selected Year: {selectedYear}</Text>
            <Text>Selected Date: {selectedDate && selectedDate.toString()}</Text>
            <Button onPress={() => setDat(new Date(2020, 7, 11))} title="CLICK" />
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default App;
