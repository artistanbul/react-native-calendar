import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  calendar: {
    backgroundColor: 'red',
    flex: 1,
    minHeight: 350,
    flexDirection: 'column',
    padding: 10
  }
});

export default styles;
