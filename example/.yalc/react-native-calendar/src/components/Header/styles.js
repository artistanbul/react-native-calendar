import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  tabPanel: {
    flex: 0.15,
    backgroundColor: 'red',
    minHeight: 30
  },
  dayNames: {
    width: '100%',
    paddingVertical: 10,
    flexDirection: 'row'
  },
  dayName: {
    width: '14%',
    textAlign: 'center'
  }
});

export default styles;
