import getDaysOfMonth from '../src/utils/getDaysOfMonth';
import { DAYS } from '../src/constants';

describe('getDaysOfMonth', () => {
    it('should return correct dayCount, prevOffset, nextOffset and prevDayCount when first day of the week is monday', () => {
        expect(getDaysOfMonth(2020, 6, DAYS.MONDAY)).toEqual({
            dayCount: 31,
            prevOffset: 2,
            nextOffset: 2,
            prevMonthDayCount: 30
        });
    });

    it('should return correct dayCount, prevOffset, nextOffset and prevDayCount when first day of the week is sunday', () => {
        expect(getDaysOfMonth(2020, 6, DAYS.SUNDAY)).toEqual({
            dayCount: 31,
            prevOffset: 3,
            nextOffset: 1,
            prevMonthDayCount: 30
        });
    });

    it('should return correct dayCount, prevOffset, nextOffset and prevDayCount when first day of the week is friday', () => {
        expect(getDaysOfMonth(2020, 6, DAYS.FRIDAY)).toEqual({
            dayCount: 31,
            prevOffset: 5,
            nextOffset: 6,
            prevMonthDayCount: 30
        });
    });
});