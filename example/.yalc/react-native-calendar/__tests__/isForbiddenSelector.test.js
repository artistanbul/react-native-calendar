import isForbiddenSelector from '../src/utils/isForbiddenSelector';

const now = new Date();
const currentMonth = now.getMonth();
const currentYear = now.getFullYear();

describe('isForbiddenSelector', () => {
    it('should give if previous scroll is forbidden', () => {
        expect(isForbiddenSelector(
            {
                currentYear: currentYear,
                currentMonth: currentMonth
            },
            {
                previousScrollRange: undefined,
                forwardScrollRange: undefined
            }
        )).toEqual([false, false]);
    });

    it('should give if previous scroll is forbidden', () => {
        expect(isForbiddenSelector(
            {
                currentYear: currentYear,
                currentMonth: currentMonth
            },
            {
                previousScrollRange: 1,
                forwardScrollRange: undefined
            }
        )).toEqual([false, false]);
    });

    it('should give if previous scroll is forbidden', () => {
        expect(isForbiddenSelector(
            {
                currentYear: currentYear,
                currentMonth: currentMonth
            },
            {
                previousScrollRange: undefined,
                forwardScrollRange: 1
            }
        )).toEqual([false, false]);
    });

    it('should give if previous scroll is forbidden', () => {
        expect(isForbiddenSelector(
            {
                currentYear: currentYear,
                currentMonth: currentMonth
            },
            {
                previousScrollRange: 1,
                forwardScrollRange: 1
            }
        )).toEqual([false, false]);
    });

    it('should give if previous scroll is forbidden', () => {
        expect(isForbiddenSelector(
            {
                currentYear: currentYear,
                currentMonth: currentMonth + 1
            },
            {
                previousScrollRange: 1,
                forwardScrollRange: 1
            }
        )).toEqual([false, true]);
    });
});